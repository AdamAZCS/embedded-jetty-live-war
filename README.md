Live/Executable War with Jetty Embedded
=======================================

This project should provide a baseline for those investigating the use of Embedded Jetty
from the point of view of a Self Executing WAR file.

The project has 3 main parts:

 2. `/theserver/` - this is the Embedded Jetty Server `jetty.livewar.ServerMain.main(String args[])` which you customize to initialize your Jetty server and its WebApp.  This project also is the place where you customize for things like JDBC servers libraries, JNDI, logging, etc.   This project produces a uber-jar with all of the dependencies needed to run the server.  Special care is taken with the `maven-shade-plugin` to merge `META-INF/services/` files.
 3. `/server-bootstrap/` - this contains 2 small classes that sets up a `LiveWarClassLoader` from the content in the live WAR and then runs `jetty.livewar.ServerMain.main(String args[])` from this new ClassLoader.  This project also contains the live `META-INF/MANIFEST.MF` that the live WAR will need/use
 4. `/livewar-assembly/` - this is the project that ties together the above 3 projects into a Live/Executable WAR file.  The artifacts from from the above 3 projects are unpacked by the `maven-assembly-plugin` and put into place where they will be most functional (and safe).  For example, the server classes from `/theserver/` are placed in `/WEB-INF/jetty-server/` to make them inaccessible from Web Clients accessing the WAR file.


This part has been removed:
 1. `/thewebapp/` - this is the WAR file, the webapp, as it exists in its native format, with normal maven `<packaging>war</packaging>` and a produced artifact that is just a WAR file that isn't (yet) self-executing.
 

Note: there are 3 files present in your new assembled WAR file that you should be aware of, as these files can be downloaded by a Web Client as static content if you use this setup.

 * `/jetty/bootstrap/JettyBootstrap.class`
 * `/jetty/bootstrap/LiveWarClassLoader.class`
 * `/META-INF/MANIFEST.MF`

The example project is setup in such a way that information present in these bootstrap files should not reveal private or sensitive information about your Server or its operations.  Merely that the Webapp can be started as a Live/Executable WAR file.

Github - Bitbucket two remote setting tracking
==============================================

this project is kind of a fork of [jetty's embedded-jetty-live-war project](https://github.com/jetty-project/embedded-jetty-live-war)

instructions taken from [this github-to-bitbucket post](https://gist.github.com/sangeeths/9467061)

in short: 

###### Go to Bitbucket and create a new repository (its better to have an empty repo).
```bash
git clone git@bitbucket.org:abc/myforkedrepo.git
cd myforkedrepo
```
###### Now add Github repo as a new remote in Bitbucket called "sync"
```bash
git remote add sync git@github.com:def/originalrepo.git
```
###### Verify what are the remotes currently being setup for "myforkedrepo".  This following command should show "fetch" and "push" for two remotes i.e. "origin" and "sync"  
```sh
git remote -v
```
###### Now do a pull from the "master" branch in the "sync" remote  
```bash
git pull sync master
```
###### up a local branch called "github"track the "sync" remote's "master" branch  
```bash
git branch --set-upstream github sync/master
```
###### push the local "master" branch to the "origin" remote in Bitbucket.  
```bash
git push -u origin master
```

Courtesy: [forking-from-github-to-bitbucket at stackoverflow.com](http://stackoverflow.com/questions/8137997/forking-from-github-to-bitbucket)

